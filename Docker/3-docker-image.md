## Understanding Docker images
### Docker Image
```text
Docker image is an executable package that includes everything needed to run an application, including the code, a runtime, libraries, environmental variables and configuration files.
Docker container is a runtime instance of an image. 
```
![img_2.png](img_2.png)

![img_4.png](img_4.png)

![img_5.png](img_5.png)

The course scope is:
![img_6.png](img_6.png)
![img_7.png](img_7.png)
### Image layers
```text
Inages are nade up of multiple read-only layers. Multiple containers  are typically based on the same image.
When an image is instantiated into a container, a top writable layer is created (which is deleted when the container is removed). 
Docker uses storage drivers to manage the contents of  the imaage layers and the writable container layer.
Each storage driver handles the implementation differently, but all drivers use stackable image layers and the copy-on-write (CoW) strategy.


```
Docker Image
![img_8.png](img_8.png)
Container is instantiated from Docker image 
![img_9.png](img_9.png)
When a container is stopped the writable layer exists.
When a container is removed the writable layer is deleted.
So container is stateless.
### Dockerfile
![img_10.png](img_10.png)

![img_11.png](img_11.png)

![img_12.png](img_12.png)
```text
The command "docker build" reads the Dockerfile from whatever directory you specify, and then builds a Docker image based on the Dockerfile
```
### Analyzing Dockerfile
```text
Dockerfile format

# Comment
INSTRUCTION arguments


 Dockerfile instructions
  - FROM [--platform=<platform>] <image>[:<tag>] [AS <name>]
             -  initializes a new build stage and sets the Base Image for subsequent instructions;
                the FROM instruction must be the first instruction in the Dockerfile.
  - ENV      - is used to define environmental variables in the container.
  - WORKDIR  - defines the working directory of the container.
  - ADD      - copies a file into the image, but supports tar and remote URL's.
  - COPY     - copy files into the image, preferred over ADD.
  - VOLUME   - creates a mount point as defined when the container is run.
  - ENTRYPOINT  - the executable to be run when the container is run.
  - CMD      - provides arguments for the entrypoint (only one is allowed).
  - EXPOSE   - documents the ports that should be published.
  - MAINTAINER  - while depriciated, it is used to document the author of the Dockerfile (typically e-mail address).
  - ONBUILD  - only used as a trigger when this image is used to build other images;
              will define commands to run "on build".
  - RUN      - will execute any commands in a new layer on top of the current image and commit the results.

docker build <dockerfile> 
docker image inspect <image id> 
docker image history <image id>   - history of changes
```
### Managing images with CLI
````text
Usage
docker image <command>
````
![img_13.png](img_13.png)
````text
docker container ls -a
docker container rm <container id>
docker rm  - remove container/s
docker rmi  - remove images
docker image prune  - remove unused images
docker image prune -a  - remove all images which are not associated with containers
````

## Managing Docker images
### Inspecting images
```text
docker image inspect [options] IMAGE
docker inspect  - extended features

docker image inspect ubuntu --format='{{.ContainerConfig.Hostname}}'
```
### Using image tagging
```text
docker tag source_image[:tag] target_image[:tag]

Multiple tags for one iamge are possible
```
### Creating image from file
```text
docker build -f ./<dockerfile_name> .  - all files in current directory (.) will be added to image
docker build -f ./<dockerfile_name> <build_context>
docker build --no-cache=true -f ./<dockerfile_name> <build_context>  - Cache is not being used

```
[Dockerfile best practices](https://docs.docker.com/develop/develop-images/dockerfile_best-practices/)
```text
Use .dockerignore
Each container should have only one concern
Sort multi-line arguments ( \ )
Link together update & install
   apt-get update && apt-get install
```
### Modifying image layers
```text
Ways to reduce image size:
1) Use squash if Docker is in experimental version
2) Export running container to file and import it as an image
```

## Docker Registry
### Deploying a registry
```text
Docker Registry is a stateless, highly scalable application that stores and lets you distribute Docker images.
Registries could be local (private) or cloud-based (private or public). 
Examples:
1. Docker Registry (local open-source)  -- read more https://docs.docker.com/registry/
    #Start your registry
    docker run -d -p 5000:5000 --name registry registry:2
    #Pull (or build) some image from the Docker Hub
    docker pull ubuntu
    #Tag the image so that it points to your registry
    docker image tag ubuntu localhost:5000/myfirstimage
    #Push it
    docker push localhost:5000/myfirstimage
    #Pull it back
    docker pull localhost:5000/myfirstimage
    #Now stop your registry and remove all data
    docker container stop registry && docker container rm -v registry
    
2. Docker Trusted Registry (DTR)
3. Docker Hub
```
### Configuring a registry
```text
If you wanna override the configuration file of the registry, you should use specifically stating the configuration of the new registry file
 that you'll be configuring or providing when you run the docker open source registry.
docker run -d -p 5000:5000 --restart=always --name registry \
             -v `pwd`/config.yml:/etc/docker/registry/config.yml \
             registry:2
```
https://docs.docker.com/registry/configuration/
### Logging int a registry
```text
To use registry you need to login there.
docker login
docker logout
```
### Push, pull, sign an image
```text
docker login
docker tag
docker push
docker image rm
docker pull

Docker Content Trust is avalable in Docker EE (image signing)
Docker Notary (image signing)
Docker EE (image scanning)
```
### Search image in registry
```text
docker search --limit=5 --filter "is-official=true" ubuntu
docker search --filter "is-official=true" --filter "stars=80" ubuntu
```
### Tagging images
```text
docker tag SOURCE_IMAGE TARGET_IMAGE

Если нужно запушить образ в публичный реестр, то нужно дать имя реестра
<registry_name>/<image_name>
mnpimenov/ubuntu
```
### Deleting images from a registry
```text
To delete on local Docker host:
docker image rm [--force] <image_name>

To delete on public registry use GUI or scripts
To delete on DTR use GUI
```
Next steps for Docker Certified Associate preparation

course Docker: Installation and Configuration

course Docker: Orchestration

DavidMDavis.com

linkedin.com/in/davidmdavis