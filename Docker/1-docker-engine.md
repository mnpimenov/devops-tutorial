## Docker Editions
```text
Community edition - free
Enterprize edition - paid, scalability, clouds...
```
### What is Docker Engine

```text
Docker Engine - client-server application consists of: 
1. Docker Server - dockerd (Docker daemon)
2. Docker daemon API - REST API (restful api)
3. Docker Client - Docker CLI 
```
### Docker architecture
![img_1.png](img_1.png)

[Понимая Docker](https://habr.com/ru/post/253877/)

[Understanding Docker](https://docs.docker.com/get-started/overview/)
```text
```
### UCP - Docker Universal Control Plane
```text
UCP, DTR
```
### Docker Namespaces
```text
Used to isolate containers' namespaces from each other and from OS.
Types of namespaces:
- Process
- Mount
- IPC
- Network
- User
```

### Docker Control groups (cgroups)
```text
cgroups are used to control resources. In Docker mostly used for container resources:
- CPU Limits
- CPU reservations
- Memory limits
- Memory reservations

To use cgroups in Docker, there are strict kernel requirements (Kernel and Linux version should be compatible)

docker run
 -c, CPU shares, № of CPU, CPUs and MEMs allocation
 -m, Memory limits, swap limit
```

## Docker installation
### Installing Docker on Linux
```text
Requirements
- Ubuntu, CentOS, Fedora, Others
- 64-bit Ubuntu
- network connected
- uninstall Docker
 "sudo apt-get remove docker docker-engine docker-ce docker.io"
- update packages
 "sudo apt-get update"
- make modifications to the Linux package installer (apt) to add  Docker repository
   allow Apt to use a repository over HTTPS
     "sudo apt-get install apt-transport-https ca-certificates curl software-properties-common"
   add the Docker official GPG key to apt
     "curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -"
   verify that you now have the Docker GPG key
     "sudo apt-key fingerprint 0EBFCD88"
   add the Docker Repository to Apt
     sudo add-apt-repository \
       "deb [arch=amd64] https://download.docker.com" \
     linux/ubuntu \
       $(lsb_release -cs) \
       stable"
   re-update the Apt Package index
     sudo apt-get update
- install a specific version of Docker / latest version
    sudo apt-get install docker-ce=17.12.0`ce-0`ubuntu
    sudo apt-get install docker-ce
- verify
    - add Groups and Users
      sudo groupadd docker
      sudo usermod -aG docker $USER
      * Log out and log back in for this to take effect
    - verify
      docker version
      docker run hello-world
```
### Installing Docker on Windows
```text
Requirements for Docker CE
- Windows 10 Pro or Ent 64-bit
```
### Installing Docker on AWS
```text
```
### Installing Docker EE on Windows Server
```text
Requirements for Docker CE
- Windows Server 2016
Being installed via Powershell
1. install docker provider
2. install docker package
```
### Upgrading Docker
```text
Go to 
 apt-get update
 
Then install Docker according to installation manual (via removal of Docker)
```

## Configuring Docker
### Docker storage drivers
```text
Usually, data is stored in Docker volumes.
Docker storage driver is important when writing data in container's writable layer.
```
[More about storage drivers](https://docs.docker.com/storage/storagedriver/select-storage-driver/)
### Docker repositories
```text
A Docker repository is a collection of related Docker images with the same name but different tags.
A registry is a place where you store your images. A registry can be public, it can be private, and it can have multiple repositories. 
Docker Hub https://hub.docker.com

    docker login
  Usually, data is stored in Docker volumes.
Docker storage driver is important when writing data in container's writable layer.
```
[More about storage drivers](https://docs.docker.com/storage/storagedriver/select-storage-driver/)

### Docker swarm (UCP)
```text
Docker Swarm is the cluster management and orchestration feature that's built into the Docker Engine. Docker Swarm is available whether or not you have Docker Community Edition or Enterprise Edition, and it's available on all platforms.
UCP dashboard for joining|removing nodes
```
![img.png](img.png)

![img_3.png](img_3.png)

### Creating & managing users & groups (UCP)
```text
UCP dashboard for management
 - Organisations & Teams
 - Users
 - Roles
 - Grants
```
### Configure Docker to start on boot
```text
How to check if Docker is enabled by default (Ubuntu)
 systemctl status docker

Disable or enable docker:
 systemctl disable docker
 systemctl enable docker
```
### Backing up Docker
```text
In production it's recommended to back up
- Docker swarm cluster
    systemctl stop docker
    cd /var/lib/docker
    copy & archive swarm directory
    systemctl start docker
- Universal Control Plane
    see exact command in Docker documentation
- Docker Trusted Registry
    - backup registry content data
    - backup DTR metadata
- Container Volume Data
    Should not be used inside your containers
    Best practices  are to keep it a container volume
    Container volume could be stored locally on Docker nodes on a share that goes to a NAS or SAN or the public cloud


Docker Swarm is the cluster management and orchestration feature that's built into the Docker Engine. Docker Swarm is available whether or not you have Docker Community Edition or Enterprise Edition, and it's available on all platforms.
UCP dashboard for joining|removing nodes
```
### Installing DTR
```text
Docker Swarm is the cluster management and orchestration feature that's built into the Docker Engine. Docker Swarm is available whether or not you have Docker Community Edition or Enterprise Edition, and it's available on all platforms.
UCP dashboard for joining|removing nodes
DTR is installed as container
```
## Troubleshooting Docker Install & Config
### Checking the status of Docker
```text
docker version
docker info
docker ps    #running containers
docker ps -a #all containers
docker node ls
```
### Configuring Docker logging
```text
Default logging driver - json-file
There're two different places to configure Docker logging that you need to be aware of.
First, there's the default log type for each Docker daemon.
Second, you can configure per-container log types that can override the default Docker daemon log.
```
### Analyzing Docker errors
```text
Common issues include:
- ensure that Linux kernel version is compatible with the version of Docker you are running
- ensure that DNS is properly configured
- ensure that the container you are running is compatible with your host operating system

Add yourself
 The command to add your user account in Linux to the Docker group, which would allow you to run Docker commands using your own user account is
 sudo usermod -aG docker $USER 

Docker daemon is not running
    sudo systemctl enable docker
    sudo systemctl start docker
```
**_Text_**
